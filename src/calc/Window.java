
package calc;


public class Window extends javax.swing.JFrame {

    
    public Window() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bNumber7 = new javax.swing.JButton();
        bNumber4 = new javax.swing.JButton();
        bNumber5 = new javax.swing.JButton();
        bNumber1 = new javax.swing.JButton();
        bNumber2 = new javax.swing.JButton();
        bNumber9 = new javax.swing.JButton();
        bNumber8 = new javax.swing.JButton();
        bNumber6 = new javax.swing.JButton();
        bNumber3 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        bOut = new javax.swing.JButton();
        bNumber0 = new javax.swing.JButton();
        bDiv = new javax.swing.JButton();
        bSub = new javax.swing.JButton();
        bAdd = new javax.swing.JButton();
        bMul = new javax.swing.JButton();
        jButton17 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Calc");
        setResizable(false);

        bNumber7.setText("7");
        bNumber7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bNumber7MouseClicked(evt);
            }
        });
        bNumber7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bNumber7ActionPerformed(evt);
            }
        });

        bNumber4.setText("4");
        bNumber4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bNumber4MouseClicked(evt);
            }
        });

        bNumber5.setText("5");
        bNumber5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bNumber5MouseClicked(evt);
            }
        });

        bNumber1.setText("1");
        bNumber1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bNumber1MouseClicked(evt);
            }
        });
        bNumber1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bNumber1ActionPerformed(evt);
            }
        });

        bNumber2.setText("2");
        bNumber2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bNumber2MouseClicked(evt);
            }
        });
        bNumber2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bNumber2ActionPerformed(evt);
            }
        });

        bNumber9.setText("9");
        bNumber9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bNumber9MouseClicked(evt);
            }
        });

        bNumber8.setText("8");
        bNumber8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bNumber8MouseClicked(evt);
            }
        });
        bNumber8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bNumber8ActionPerformed(evt);
            }
        });

        bNumber6.setText("6");
        bNumber6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bNumber6MouseClicked(evt);
            }
        });

        bNumber3.setText("3");
        bNumber3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bNumber3MouseClicked(evt);
            }
        });

        jButton10.setText(".");
        jButton10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton10MouseClicked(evt);
            }
        });
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        bOut.setText("=");
        bOut.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                bOutMouseWheelMoved(evt);
            }
        });
        bOut.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bOutMouseClicked(evt);
            }
        });

        bNumber0.setText("0");
        bNumber0.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bNumber0MouseClicked(evt);
            }
        });
        bNumber0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bNumber0ActionPerformed(evt);
            }
        });

        bDiv.setText("/");
        bDiv.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bDivMouseClicked(evt);
            }
        });

        bSub.setText("-");
        bSub.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bSubMouseClicked(evt);
            }
        });

        bAdd.setText("+");
        bAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bAddMouseClicked(evt);
            }
        });

        bMul.setText("*");
        bMul.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bMulMouseClicked(evt);
            }
        });

        jButton17.setText("CE");
        jButton17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton17MouseClicked(evt);
            }
        });

        jTextField1.setText("0");

        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jLabel1.setText("Wynik");

        jLabel2.setText("Działanie");

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(bNumber0, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(bOut, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(6, 6, 6))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(bNumber7, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(bNumber8, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(bNumber9, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(bNumber4, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(bNumber5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(bNumber6, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(bNumber1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(bNumber2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(bNumber3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(bSub, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(bMul, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(bAdd, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(bDiv, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jTextField1)
                            .addComponent(jTextField2)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(124, 124, 124)
                                .addComponent(jButton17))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bNumber7, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bNumber8, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bNumber9, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bDiv, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bNumber4, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bNumber5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bNumber6, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bSub, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bNumber1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bNumber2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bNumber3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bNumber0, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bOut, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bMul, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bNumber7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bNumber7ActionPerformed
        
    }//GEN-LAST:event_bNumber7ActionPerformed

    private void bNumber7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bNumber7MouseClicked
        logic.bNumberLogic("7", clicked);
        text=text+"7";
        jTextField1.setText(text);
    }//GEN-LAST:event_bNumber7MouseClicked

    private void bNumber8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bNumber8MouseClicked
        logic.bNumberLogic("8", clicked);
        text=text+"8";
        jTextField1.setText(text);
    }//GEN-LAST:event_bNumber8MouseClicked

    private void bNumber9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bNumber9MouseClicked
        logic.bNumberLogic("9", clicked);
        text=text+"9";
        jTextField1.setText(text);
    }//GEN-LAST:event_bNumber9MouseClicked

    private void bDivMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bDivMouseClicked
       if(clicked == false){
            operation="div";
            text=text+" / ";
            jTextField1.setText(text);
        }else{
            jTextField2.setText("juz wybrales byczq");
        }
        clicked = true;
    }//GEN-LAST:event_bDivMouseClicked

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        
    }//GEN-LAST:event_jButton10ActionPerformed

    private void bNumber1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bNumber1ActionPerformed
        
    }//GEN-LAST:event_bNumber1ActionPerformed

    private void bNumber2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bNumber2ActionPerformed
        
    }//GEN-LAST:event_bNumber2ActionPerformed

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void bNumber0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bNumber0ActionPerformed
        
    }//GEN-LAST:event_bNumber0ActionPerformed

    private void bNumber1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bNumber1MouseClicked
        logic.bNumberLogic("1", clicked);
        text=text+"1";
        jTextField1.setText(text);
    }//GEN-LAST:event_bNumber1MouseClicked

    private void bNumber2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bNumber2MouseClicked
        logic.bNumberLogic("2", clicked);
        text=text+"2";
        jTextField1.setText(text);
    }//GEN-LAST:event_bNumber2MouseClicked

    private void bNumber3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bNumber3MouseClicked
        logic.bNumberLogic("3", clicked);
        text=text+"3";
        jTextField1.setText(text);
    }//GEN-LAST:event_bNumber3MouseClicked

    private void bNumber4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bNumber4MouseClicked
        logic.bNumberLogic("4", clicked);
        text=text+"4";
        jTextField1.setText(text);
    }//GEN-LAST:event_bNumber4MouseClicked

    private void bNumber5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bNumber5MouseClicked
        logic.bNumberLogic("5", clicked);
        text=text+"5";
        jTextField1.setText(text);
    }//GEN-LAST:event_bNumber5MouseClicked

    private void bNumber6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bNumber6MouseClicked
        logic.bNumberLogic("6", clicked);
        text=text+"6";
        jTextField1.setText(text);
    }//GEN-LAST:event_bNumber6MouseClicked

    private void bNumber0MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bNumber0MouseClicked
        logic.bNumberLogic("0", clicked);
        text=text+"0";
        jTextField1.setText(text);
    }//GEN-LAST:event_bNumber0MouseClicked

    private void jButton10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton10MouseClicked
        logic.bNumberLogic(".", clicked);
        text=text+".";
        jTextField1.setText(text);
    }//GEN-LAST:event_jButton10MouseClicked

    private void bAddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bAddMouseClicked
        if(clicked == false){
            operation="add";
            text=text+" + ";
            jTextField1.setText(text);
        }else{
            jTextField2.setText("juz wybrales byczq");
        }
        clicked = true;
    }//GEN-LAST:event_bAddMouseClicked

    private void bOutMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_bOutMouseWheelMoved
          
    }//GEN-LAST:event_bOutMouseWheelMoved

    private void bOutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bOutMouseClicked
        jTextField2.setText(logic.result(operation));  
    }//GEN-LAST:event_bOutMouseClicked

    private void bMulMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bMulMouseClicked
        if(clicked == false){
            operation="mul";
            text=text+" * ";
            jTextField1.setText(text);
        }else{
            jTextField2.setText("juz wybrales byczq");
        }
        clicked = true;
    }//GEN-LAST:event_bMulMouseClicked

    private void bSubMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bSubMouseClicked
        if(clicked == false){
            operation="subb";
            text=text+" - ";
            jTextField1.setText(text);
        }else{
            jTextField2.setText("juz wybrales byczq");
        }
        clicked = true;
    }//GEN-LAST:event_bSubMouseClicked

    private void jButton17MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton17MouseClicked
        clicked=false;
        logic.clean();
        text="";
        jTextField1.setText("");
        jTextField2.setText("");
    }//GEN-LAST:event_jButton17MouseClicked

    private void bNumber8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bNumber8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bNumber8ActionPerformed

    
    
    
    Logic logic = new Logic();
    boolean clicked=false;
    String text="";
    String operation;
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bAdd;
    private javax.swing.JButton bDiv;
    private javax.swing.JButton bMul;
    private javax.swing.JButton bNumber0;
    private javax.swing.JButton bNumber1;
    private javax.swing.JButton bNumber2;
    private javax.swing.JButton bNumber3;
    private javax.swing.JButton bNumber4;
    private javax.swing.JButton bNumber5;
    private javax.swing.JButton bNumber6;
    private javax.swing.JButton bNumber7;
    private javax.swing.JButton bNumber8;
    private javax.swing.JButton bNumber9;
    private javax.swing.JButton bOut;
    private javax.swing.JButton bSub;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton17;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}
